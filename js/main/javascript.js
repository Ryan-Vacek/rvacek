var home = $('#home .main'); // Home page

var params, postTemplate, html;

if (home) {
	params = {
		'_hero': { // Hero module
			'id': '',
			'class': '',
			'introText': 'Lorem Ipsum',
			'img': {
				'src': 'https://placeholdit.imgix.net/~text?txtsize=23&txt=2000x700&w=2000&h=700',
				'alt': 'Placeholder'
			}
		},
		'_text_columns': { // Text columns module (2 columns in this example)
			'id': 'home-text-cols',
			'class': '',
			'columns': [
				{
					'id': 'col-1',
					'class': '',
					'text': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eleifend ultrices lectus, quis maximus neque pharetra vel. Vestibulum tempor maximus mi id feugiat. Nulla quis nibh velit. Maecenas interdum vestibulum dui, quis rutrum nunc blandit ac. Morbi tempor sagittis vehicula. Phasellus malesuada laoreet tincidunt. Pellentesque et viverra justo. Donec nulla dui, sagittis quis volutpat et, egestas vel nulla.'
				},
				{
					'id': '',
					'class': '',
					'text': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eleifend ultrices lectus, quis maximus neque pharetra vel. Vestibulum tempor maximus mi id feugiat. Nulla quis nibh velit. Maecenas interdum vestibulum dui, quis rutrum nunc blandit ac. Morbi tempor sagittis vehicula. Phasellus malesuada laoreet tincidunt. Pellentesque et viverra justo. Donec nulla dui, sagittis quis volutpat et, egestas vel nulla.'
				}
			]
		}
	};
	postTemplate = JST.index;
	html = postTemplate(params);
	home.append(html);
}