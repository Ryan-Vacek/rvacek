# Ryan Vacek Sample Site
A sample site, with the build done in Grunt and the templating done in Handlebars.

### Installation & Use

#### Installation

- Clone the repo and navigate to its directory
- Run 'npm install'

#### Use

- To build, run 'grunt'
- The sample page can then be accessed at http://localhost:9000/index.html