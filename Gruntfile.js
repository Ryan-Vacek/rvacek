var build_state = 'prod',
	sass_files = {
		"css/style.min.css": "css/sass/style.scss"
	},
	watched_sass_files = [ 'css/sass/**/*/*.scss' ],
	uglify_source_files = [
		'js/vendor/**/*.js',
		'js/main/javascript.js',
	],
	uglify_files = {
		'js/javascript.min.js': uglify_source_files
	},
	watched_js_files = [
		'js/vendor/**/*.js',
		'js/main/*.js',
	],
	watched_hbs_files = [
		'templates/**/*.hbs',
	];

module.exports = function(grunt) {
	"use strict";

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		jshint: { // Ignoring templates.js and the minified site JS due to jshint spitting out errors caused by the build tools -- any JS entered by developers will still be checked
			files: ['Gruntfile.js', 'js/**/*.js', '!js/templates.js', '!js/javascript.min.js'],
			options: {
				globals: {
					jQuery: true
				}
			}
		},
		watch: {
			sass: {
				files: watched_sass_files,
				tasks: ["sass:"+build_state],
				options: {
					livereload: true
				}
			},
			js: {
				files: watched_js_files,
				tasks: ["uglify:"+build_state],
				options: {
					livereload: true
				}
			},
			handlebars: {
				files: watched_hbs_files,
				tasks: ["handlebars:"+build_state],
				options: {
					livereload: true
				}
			}
		},
		uglify: {
			dev: {
				files: uglify_files,
				options: {
					beautify: true,
					mangle: false
				}
			},
			prod: {
				files: uglify_files
			},
			releaseUnmin: {
				files: {
					'js/main/javascript.js': uglify_source_files
				},
				options: {
					beautify: true,
					mangle: false
				}
			}
		},
		sass: {
			dev: {
				options: {
					outputStyle: "nested",
					sourceMap: true
				},
				files: sass_files,
			},
			prod: {
				options: {
					outputStyle: "compressed",
					sourceMap: true
				},
				files: sass_files,
			},
			releaseUnmin: {
				options: {
					style: "normal"
				},
				files: {
					"css/style.css": "css/sass/style.scss"
				},
			}
		},
		handlebars: {
			options: {
				namespace: 'JST',
				partialRegex: /^_/,
				processName: function(filePath) {
					return filePath.replace(/^templates\//, '').replace(/partials\//, '').replace(/\.hbs$/, '').replace(/_/, '');
				}
			},
			prod: {
				files: {
					"js/templates.js": ["templates/**/*.hbs"]
				}
			},
			dev: {
				files: {
					"js/templates.js": ["templates/**/*.hbs"]
				}
			}
		},
		serve: {
			options: {
				port: 9000,
				livereload: true,
				hostname: 'localhost',
				base: 'index.html'
			},
			livereload: {
				options: {
					open: true,
					base: ['pages/']
				}
			}
		},
		concurrent: {
			options: {
				logConcurrentOutput: true
			},
			prod: {
				tasks: ['serve', 'watch']
			},
			dev: {
				tasks: ['serve', 'watch']
			}
		}
	});

	grunt.registerTask('default', [build_state]);
	grunt.registerTask('prod', ['jshint', 'sass:prod', 'uglify:prod', 'handlebars:prod', 'concurrent:prod']);
	grunt.registerTask('dev', ['jshint', 'sass:dev', 'uglify:dev', 'handlebars:prod', 'concurrent:dev']);

	grunt.loadNpmTasks("grunt-sass");
	grunt.loadNpmTasks("grunt-serve");
	grunt.loadNpmTasks("grunt-concurrent");
	grunt.loadNpmTasks("grunt-contrib-handlebars");
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks('grunt-contrib-jshint');
};